import requests

#https://api.tori.fi/api/v1.2/public/ads?ad_type=s
url = "https://api.tori.fi/api/v1.2/public/ads"
querystring = {"ad_type":"s","category":"5038","q":"ryzen","region":"16","sort":"date"}
response = requests.request("GET", url, params=querystring)


for ilmoitus in response.json()['list_ads']:
    otsikko = ilmoitus['ad']['subject']
    hinta = ilmoitus['ad']['prices'][0]['price_value']
    aika = ilmoitus['ad']['list_time']['label']
    print(otsikko, hinta, aika)

