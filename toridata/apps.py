from django.apps import AppConfig


class ToridataConfig(AppConfig):
    name = 'toridata'
